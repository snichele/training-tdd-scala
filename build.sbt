name := "scalacheck-demo"

scalaVersion := "2.11.7"

libraryDependencies += "org.scalatest" %% "scalatest" % "2.2.6" % "test"

testOptions in Test += Tests.Argument(
TestFrameworks.ScalaCheck,
"-maxSize", "5",
"-minSuccessfulTests", "33",
"-workers", "1",
"-verbosity", "1"
)
